$(document).ready(function () {
  var main = new Main()
  var clipboard = new Clipboard('.copy-btn')
  $('#error-alert').hide()
})

var Main = function () {
  this.bindUIEvents() // DOM events
  this.data = {}
  this.apiUrl = 'http://127.0.0.1:3000'
}

Main.prototype.bindUIEvents = function () {
  var self = this

  $('input[type=radio][name=platform]').change(function (e) {
    console.log('e is ', e)
    self.showClientSection(e)
  })

  $('input[type=radio][name=client]').change(function (e) {
    e.preventDefault()
    $('.last-section').removeClass('hidden')
    $('#submit-section').removeClass('hidden')
  })

  $('#toggle-help-text').click(function (e) {
    e.preventDefault()
    $('#platform-help-text').toggle('hidden')
  })

  $('#submit-form').click(function (e) {
    e.preventDefault()
    self.submit()
  })

  $('#new-appkey-btn').click(function (e) {
    e.preventDefault()
    self.toggleCodeSnippet()
    document.getElementById('questions').reset()
    $('.last-section').addClass('hidden')
    $('.clients').addClass('hidden')
    $('#client-section').addClass('hidden')
    $('#submit-section').addClass('hidden')
  })

  $('#developerId').change(function (e) {
    $('#developerId').removeClass('error')
  })

  $('#applicationId').change(function (e) {
    $('#applicationId').removeClass('error')
  })
}

Main.prototype.showClientSection = function (e) {
  var platform = e.currentTarget.value
  $('.last-section').addClass('hidden')
  $('#submit-section').addClass('hidden')
  $('#platform-help-text').addClass('hidden')
  $('.clients').addClass('hidden')
  $('#client-section').removeClass('hidden')
  $('#client-' + platform).removeClass('hidden')
  this.data['platform'] = platform
  this.setHelpText(platform)
}

Main.prototype.setHelpText = function (platform) {
  helpTexts = {
    'android': `Your application id should be defined in your app module's
                    build.gradle - e.g. app/build.gradle - and is located inside the
                    android.defaultConfig configuration. Your application id should be a
                    reverse-domain formatted string - such as, com.mycompany.myproductname.`,
    'ios': `For iOS applications, use your Bundle ID. To find your bundle ID in Xcode:
                In the project navigator, select the project and your target to display the
                project editor. Click General and, if necessary, click the disclosure triangle
                next to Identity to reveal the settings. Your bundle identifier should be a
                reverse-domain formatted string - such as, com.MyCompany.MyProductName.`,
    'windows': `For UWP applications, use the identity name from your app package manifest.
                The <Identity> element is found inside the top-level <Package> element of
                Package.appxmanifest file. Your identity name should be an string consisting
                of alpha-numeric, period and dash characters - such as, Microsoft.SDKSamples.ApplicationDataSample.`,
    'cross-platform': 'TBD'
  }

  $('#platform-help-text').text(helpTexts[platform])
}

Main.prototype.toggleCodeSnippet = function (e) {
  if ($('#questions').hasClass('hidden')) {
    $('#questions').removeClass('hidden')
    $('#codeSnippet').addClass('hidden')
  } else {
    $('#questions').addClass('hidden')
    $('#codeSnippet').removeClass('hidden')
  }
}

Main.prototype.displayErrorMessage = function (msg) {
  $('#error-message').text(msg)
  $('#error-alert').fadeTo(2000, 500).slideUp(500, function () {
    $('#error-alert').slideUp(500)
  })
}

Main.prototype.submit = function () {
  var self = this
  var developerId = $('#developerId').val()
  var applicationId = $('#applicationId').val()
  if (!developerId) {
    $('#developerId').addClass('error')
  }
  if (!applicationId) {
    $('#applicationId').addClass('error')
  }
  if (!developerId || !applicationId) {
    return false
  }
  $('#loader').show()

  $.ajax({
    type: 'GET',
    url: self.apiUrl + '?appId=' + this.data['platform'] + ':' + applicationId + '&developerId=' + developerId,
    crossDomain: true,
    datatype: 'jsonp',
    success: function (data) {
      $('#loader').hide()
      if (data.error) {
        self.displayErrorMessage(data.error)
      } else {
        if (data.appKey) {
          $('.appkey-value').text(data.appKey)
          self.toggleCodeSnippet()
        } else {
          self.displayErrorMessage('Could not retrieve App Key')
        }
      }
    },
    error: function (error) {
      $('#loader').hide()
      console.log('Error is ', error)
      self.displayErrorMessage(error.statusText)
    }
  })
}
